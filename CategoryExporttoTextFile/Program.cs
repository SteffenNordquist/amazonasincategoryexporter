﻿using DN_Classes.AppStatus;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryExporttoTextFile
{
    class Program
    {
        public static MongoCollection<CategoriesEntity> Category = new MongoClient(@"mongodb://client148:client148devnetworks@136.243.44.111/AmazonAsinCategories").GetServer().GetDatabase("AmazonAsinCategories").GetCollection<CategoriesEntity>("categories");
       public static ApplicationStatus appstatus = new ApplicationStatus("CategoryExporttoTextFile148");

        static void Main(string[] args)
        {
            appstatus.Start();

            try
            {
                Console.WriteLine("CategoryExporttoTextFile148");
                GetCategories();
                appstatus.Stop();
            }
            catch (Exception ex){
                appstatus.AddMessagLine(ex.StackTrace);
                appstatus.Stop();
               
            }
        }

        public static void GetCategories()
        {
            var categories = Category.FindAll();
            var categoryLines = categories.Select(x => string.Format("{0}\t{1}", x.id, x.category)).ToList();
            File.WriteAllLines(Properties.Settings.Default.ExportPath, categoryLines);
            appstatus.Successful();
        }
    }
}